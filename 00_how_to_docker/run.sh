#!/bin/bash

YEL='\033[1;33m'   # Yellow
ORA='\033[1;214m'  # Orange
GRE='\033[0;32m'   # Green
RED='\033[0;31m'   # Red
NC='\033[0m' # No Color
DEBUG=0

step=$1

# Take one arg, and int, return 1 if variable is an INT or 0 if not
function	ft_is_int()
{
	# If DEBUG>1 print info
	if [ $DEBUG -gt 1 ]
	then
		echo "Enter in is_int() with '`echo $1`' value"
	fi

	# Check if value is numeric
	if [ "$1" -eq "$1" ] 2>/dev/null
	then
		return 0
	else
		echo -e ${RED}"Value '`echo $1`' isn't numeric"${NC}
		return 1
	fi
}

echo "Run all exercices"

if [ ${#1} -ne 0 ]
then
	exercices="$(ls -1 | tail -n +$1)"
else
	exercices="$(ls -1)"
fi

for i in "${!exercices[@]}"
do
	echo "ex: ${exercices[@]}"
	if ft_is_int ${exercices[$i]}
	then
		echo -e ${ORA}"zsh ${exercices[$i]}"${NC}
		zsh `echo ${exercices[$i]}`
		if [ "$(echo $?)" -eq 1 ]
		then
			echo -e ${RED}"Cant't run '${exercices[$i]}' commands"${NC}
			exit 1
		fi
	fi
done
